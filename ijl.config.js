const pkg = require('./package')

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        'some-test.main': '/some-test',
    },
    features: {
        'some-test': {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        'some-test.api': '/api',
    }
}
