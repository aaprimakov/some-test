import React, { useEffect, useState, useMemo } from 'react';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import Toolbar from '@mui/material/Toolbar';
import CircularProgress from '@mui/material/CircularProgress';
import FormControl from '@mui/material/FormControl';
import Lottie from 'react-lottie';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import dayjs from 'dayjs';
import {
  Chart,
  BarSeries,
  Title,
  ArgumentAxis,
  ValueAxis,
} from '@devexpress/dx-react-chart-material-ui';
import { Animation } from '@devexpress/dx-react-chart';

import { anims } from './animations';

import { useCountriesQuery, useLazyStatusQuery } from './api';

export const Dashboard = () => {
    const [country, setCounty] = useState(null);
    const [status, setStatus] = useState('Confirmed');
    const [isPlaying, setIsPlaying] = useState(true);
    const { data, isUninitialized, isLoading } = useCountriesQuery(void 0);
    const [getStatuses, statuses] = useLazyStatusQuery();

    const handleChangeCountry = (event) => {
        setCounty(event.target.value);
    };

    const handleChangeStatus = (event) => {
        setStatus(event.target.value);
    };

    useEffect(() => {
        if (country) {
            getStatuses({ countrieName: country, status })
        }
    }, [country, status])

    const handleAnimEnds = () => {
        setIsPlaying(false)
    }

    const anumIndex = useMemo(() => Math.random() * 10 > 5 ? 0 : 1, [])
    
    if (isPlaying) {
        
        return (<Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <Typography variant='h2'>{anims[anumIndex].title}</Typography>
            <Lottie
                options={{
                    loop: true,
                    autoplay: true,
                    animationData: anims[anumIndex].lottie,
                    rendererSettings: {
                        preserveAspectRatio: 'xMidYMid slice',
                    },
                }}
                eventListeners={[
                    {
                        eventName: 'loopComplete',
                        callback: handleAnimEnds,
                    },
                ]}
                height={400}
                width={400}
            /></Box>
        )
    }

    if (isLoading || isUninitialized) {
        return <CircularProgress />
    }

    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography>Ситуация с COVID 19</Typography>
                    </Toolbar>
                </AppBar>
            </Box>
            <Box sx={{ padding: 2 }}>
                <div>
                    <FormControl variant="standard" sx={{ m: 1, minWidth: '440px' }}>
                        <InputLabel id="countries-select">Страна</InputLabel>
                        <Select
                            labelId="countries-select"
                            value={country}
                            onChange={handleChangeCountry}
                        >
                            {data.map(country => (
                                <MenuItem key={country.Country} value={country.Country}>{country.Country}</MenuItem>
                                ))}
                        </Select>
                    </FormControl>
                    <FormControl variant="standard" sx={{ m: 1, minWidth: '440px' }}>
                        <InputLabel id="status-select">Cтатус</InputLabel>
                        <Select
                            labelId="status-select"
                            value={status}
                            onChange={handleChangeStatus}
                        >
                            <MenuItem value="Confirmed">Confirmed</MenuItem>
                            <MenuItem value="Deaths">Deaths</MenuItem>
                            <MenuItem value="Recovered">Recovered</MenuItem>
                            <MenuItem value="Active">Active</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <Paper>
                    {statuses.isLoading && <CircularProgress />}
                    {statuses.isSuccess && (
                        <Chart
                            data={statuses.data.map(d => ({ ...d, date: dayjs(d.Date).format('DD:MM') }))}
                        >
                            <ArgumentAxis />
                            <ValueAxis />

                            <BarSeries
                                valueField="Cases"
                                argumentField="date"
                            />
                            <Title text="Статистика по COVID-19 за последние 30 дней" />
                            <Animation />
                        </Chart>
                    )}
                </Paper>
            </Box>
        </>
    );
};
