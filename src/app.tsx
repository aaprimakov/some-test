import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ApiProvider } from '@reduxjs/toolkit/query/react';

import { api } from './api';

import { Dashboard } from './dashboard';

const App = () => {
    return(
        <ApiProvider api={api}>
            <Dashboard />
        </ApiProvider>
    )
}

export default App;

