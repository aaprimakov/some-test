import { default as hands } from './hands.json';
import { default as clean } from './clean.json';

export const anims = [
    {
        lottie: hands,
        title: 'Не забывайте мыть руки'
    },
    {
        lottie: clean,
        title: 'Обрабатывайте руки в общественных местах'
    }
]