import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import dayjs from 'dayjs';

export const api = createApi({
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://api.covid19api.com'
    }),
    reducerPath: 'api',
    endpoints: builder => ({
        countries: builder.query({
            query: () => ({
                url: '/countries'
            })
        }),
        status: builder.query({
            query: ({ countrieName, status }) => ({
                url: `/country/${countrieName}/status/${status.toLowerCase()}?from=2021-${
                    dayjs().subtract(30, 'day').format('MM-DD')
                }T00:00:00Z&to=2022-${dayjs().format('MM-DD')}T00:00:00Z`
            })
        })
    })
})

export const { useCountriesQuery, useLazyStatusQuery } = api;
